package com.verto.challenge.model.domain

/**
 * Simple wrapper around coordinates for simplicity
 * @param latitude  latitude
 * @param longitude longitude
 * @constructor wraps location
 * @author novikov
 *         Date: 14.09.2017
 */
data class WikiLocation(
        val latitude: Double,
        val longitude: Double
)