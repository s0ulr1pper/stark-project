package com.verto.challenge.model.domain

import org.apache.commons.text.similarity.LevenshteinDistance

/**
 * Simple representation of levenshtein distance
 * @param leftWord  left word
 * @param rightWord right word
 * @author novikov
 *         Date: 14.09.2017
 */
data class LevenshteinEntity(val leftWord: String,
                             val rightWord: String) {

    /** Levenshtein distance between [leftWord] and [rightWord] */
    val levenshteinDistance: Int = LevenshteinDistance.getDefaultInstance().apply(leftWord, rightWord)

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }
        if (other == null || javaClass != other.javaClass) {
            return false
        }

        val entity = other as LevenshteinEntity

        if (leftWord != entity.leftWord) {
            return false
        }
        return rightWord == entity.rightWord
    }

    override fun hashCode(): Int {
        var result = leftWord.hashCode()
        result = 31 * result + rightWord.hashCode()
        return result
    }
}