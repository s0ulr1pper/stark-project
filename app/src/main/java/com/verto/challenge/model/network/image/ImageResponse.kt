package com.verto.challenge.model.network.image

import com.google.gson.annotations.SerializedName

/**
 * A set of image data classes for gson
 * @author novikov
 *         Date: 14.09.2017
 */
data class ImageResponse(
        @SerializedName("query") var query: ImageQuery?
)

data class ImageQuery(
        @SerializedName("pages") var pages: Map<String, Page>?
)

data class Page(
        @SerializedName("pageid") var pageId: String?,
        @SerializedName("images") var images: List<Image>?
)

data class Image(
        @SerializedName("title") var title: String?
)
