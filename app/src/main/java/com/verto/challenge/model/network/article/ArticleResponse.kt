package com.verto.challenge.model.network.article

import com.google.gson.annotations.SerializedName

/**
 * A set of article data classes for gson
 * @author novikov
 *         Date: 14.09.2017
 */
data class Article(
        @SerializedName("pageid") var pageId: String?
)

data class ArticleQuery(
        @SerializedName("geosearch") var articles: List<Article>?
)

data class ArticleResponse(
        @SerializedName("query") var query: ArticleQuery?
)