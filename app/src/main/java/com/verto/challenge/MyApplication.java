package com.verto.challenge;

import android.app.Application;
import com.verto.challenge.injection.ApplicationComponent;
import com.verto.challenge.injection.ApplicationModule;
import com.verto.challenge.injection.DaggerApplicationComponent;

/**
 * Application
 * @author novikov
 *         Date: 14.09.2017
 */
public class MyApplication extends Application {

    /** Application component */
    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationComponent = DaggerApplicationComponent.builder().applicationModule(new ApplicationModule(this)).build();
    }

    /**
     * Application component getter
     * @return application component
     */
    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
