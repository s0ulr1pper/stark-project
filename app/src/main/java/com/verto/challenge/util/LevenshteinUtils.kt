package com.verto.challenge.util

import com.verto.challenge.model.domain.LevenshteinEntity

/**
 * Append levenshtein threshold for current list of levenshtein entities
 * @param threshold levenshtein threshold
 * @return sort descending and filter levenshtein entity list
 */
fun <T : Iterable<LevenshteinEntity>> T.appendLevenshteinThreshold(threshold: Int): List<LevenshteinEntity> {
    return this.sortedWith(Comparator { lhs, rhs -> rhs.levenshteinDistance - lhs.levenshteinDistance }).filter { it.levenshteinDistance < threshold }
}