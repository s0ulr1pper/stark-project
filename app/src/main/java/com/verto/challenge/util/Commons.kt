package com.verto.challenge.util

/**
 * Just a TAG
 * @author novikov
 *         14.09.2017
 */
class Commons {

    companion object {

        /** TAG */
        val TAG: String = "VERTO"

    }
}