package com.verto.challenge.util

import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity

/**
 * Utility class for convenient purposes
 * @author novikov
 *         14.09.2017
 */

/**
 * Requests specific permission and invokes an action if already granted
 * @param permission                 permission requested
 * @param requestCode                permission request code
 * @param onPermissionAlreadyGranted action to invoke if already granted
 */
fun <T> T.actionWithPermission(permission: String, requestCode: Int, onPermissionAlreadyGranted: () -> Unit) where T : android.support.v4.app.ActivityCompat.OnRequestPermissionsResultCallback, T : AppCompatActivity {
    if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
        ActivityCompat.requestPermissions(this, arrayOf(permission), requestCode)
    } else {
        onPermissionAlreadyGranted()
    }
}

/**
 * convenient function to check if permission is already granted
 */
fun AppCompatActivity.isPermissionGranted(granted: IntArray): Boolean {
    return granted.isNotEmpty() && granted[0] == PackageManager.PERMISSION_GRANTED
}
