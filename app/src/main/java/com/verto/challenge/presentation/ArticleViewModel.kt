package com.verto.challenge.presentation

import android.databinding.BaseObservable
import android.databinding.BindingAdapter
import android.databinding.ObservableArrayList
import android.databinding.ObservableList
import android.util.Log
import android.widget.TextView
import com.verto.challenge.R
import com.verto.challenge.domain.interactor.WikipediaInteractor
import com.verto.challenge.injection.scope.PerActivity
import com.verto.challenge.model.domain.LevenshteinEntity
import com.verto.challenge.model.domain.WikiLocation
import com.verto.challenge.util.Commons
import com.verto.challenge.util.appendLevenshteinThreshold
import com.verto.challenge.view.ArticleView
import io.reactivex.observers.DisposableMaybeObserver
import javax.inject.Inject

/**
 * View model
 * @author novikov
 *         Date: 14.09.2017
 */
@PerActivity
class ArticleViewModel @Inject constructor(
        private val wikipediaInteractor: WikipediaInteractor
) : BaseObservable() {

    /**
     * Levenshtein threshold.
     * Modify this value to increase/decrease strictness of results
     * Bigger value goes for less strict results
     */
    private val threshold: Int = 15

    /** View to interact with */
    private var articleView: ArticleView? = null

    /** Location */
    private var location: WikiLocation? = null

    /** A list of images to render */
    val images: ObservableList<LevenshteinEntity> = ObservableArrayList()

    /**
     * Attach view
     * @param articleView article view to interact
     */
    fun attachView(articleView: ArticleView) {
        this.articleView = articleView
    }

    /** HULK SMASH */
    fun destroy() {
        articleView = null
        wikipediaInteractor.dispose()
    }

    /** Responds to location request action */
    fun onLocationRequested() {
        wikipediaInteractor.requestLocation(LocationResponseObserver())
    }


    /** Location response observer. In ideal world should be in separate file. */
    inner class LocationResponseObserver : DisposableMaybeObserver<WikiLocation>() {

        override fun onComplete() {
            Log.d(Commons.TAG, "LocationResponseObserver onComplete")
        }

        override fun onError(e: Throwable) {
            Log.e(Commons.TAG, "LocationResponseObserver onError", e)
            articleView?.onError(e.message)
        }

        override fun onSuccess(location: WikiLocation) {
            Log.d(Commons.TAG, "LocationResponseObserver onSuccess")
            this@ArticleViewModel.location = location
            wikipediaInteractor.requestArticles(ArticleResponseObserver(), location!!)
        }
    }

    /** Article response observer. In ideal world should be in separate file. */
    inner class ArticleResponseObserver : DisposableMaybeObserver<List<String>>() {

        override fun onComplete() {
            Log.d(Commons.TAG, "ArticleResponseObserver onComplete")
        }

        override fun onError(e: Throwable) {
            Log.e(Commons.TAG, "ArticleResponseObserver onError", e)
            articleView?.onError(e.message)
        }

        override fun onSuccess(imageIds: List<String>) {
            Log.d(Commons.TAG, "ArticleResponseObserver onSuccess")
            wikipediaInteractor.requestImages(ImageResponseObserver(), imageIds)
        }
    }

    /** Image response observer. In ideal world should be in separate file. */
    inner class ImageResponseObserver : DisposableMaybeObserver<List<LevenshteinEntity>>() {

        override fun onComplete() {
            Log.d(Commons.TAG, "ImageResponseObserver onComplete")
        }

        override fun onError(e: Throwable) {
            Log.e(Commons.TAG, "ImageResponseObserver onError", e)
            articleView?.onError(e.message)
        }

        override fun onSuccess(list: List<LevenshteinEntity>) {
            Log.d(Commons.TAG, "ImageResponseObserver onSuccess $list")
            images.clear()
            images.addAll(list.appendLevenshteinThreshold(threshold))
            articleView?.onSuccess()
        }
    }

    companion object {

        /** Binds list of levenshtein entites into text view */
        @BindingAdapter("app:entries")
        @JvmStatic
        fun setEntries(view: TextView, list: List<LevenshteinEntity>) {
            if (list.isEmpty()) {
                view.text = view.resources.getString(R.string.nothing_found)
                return
            }
            val result = list.joinToString(System.lineSeparator(), transform = { "${it.leftWord} | ${it.rightWord} | ${it.levenshteinDistance}" })
            view.text = result
        }
    }

}