package com.verto.challenge.domain.interactor

import dagger.internal.Preconditions
import io.reactivex.Maybe
import io.reactivex.MaybeObserver
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import com.verto.challenge.domain.use_case.AbstractUseCase
import java.util.concurrent.Executor

/**
 * Abstract interactor
 * @param threadExecutor      thread for execution
 * @param postExecutionThread thread for delivering results
 * @constructor               default constructor with required parameters
 * @author novikov
 *         Date: 14.09.2017
 */
abstract class AbstractInteractor(
        private val threadExecutor: Executor,
        private val postExecutionThread: Scheduler
) {

    /** Composite disposable */
    private val disposables: CompositeDisposable = CompositeDisposable()

    /**
     * Default execution method
     * @param observer observer
     * @param useCase  use case
     * @param params   use case parameters
     * @return maybe observable
     */
    fun <T, Params> execute(observer: MaybeObserver<T>, useCase: AbstractUseCase<T, Params>, params: Params): Maybe<T> {
        val maybe = useCase.buildUseCaseObservable(params)
                .subscribeOn(Schedulers.from(threadExecutor))
                .observeOn(postExecutionThread)
        maybe.doOnSubscribe { addDisposable(it) }
        maybe.subscribeWith(observer)
        return maybe
    }

    /** Dispose from current [disposables] */
    fun dispose() {
        if (!disposables.isDisposed) {
            disposables.dispose()
        }
    }

    /** Add disposable */
    private fun addDisposable(disposable: Disposable) {
        Preconditions.checkNotNull(disposable)
        Preconditions.checkNotNull(disposables)
        disposables.add(disposable)
    }

}