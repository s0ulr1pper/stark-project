package com.verto.challenge.domain.use_case

import com.verto.challenge.injection.qualifier.OnlineDataSource
import com.verto.challenge.injection.scope.PerActivity
import io.reactivex.Maybe
import com.verto.challenge.domain.mapper.ArticleResponseMapper
import com.verto.challenge.model.domain.WikiLocation
import com.verto.challenge.network.WikipediaRepository
import javax.inject.Inject

/**
 * Request articles use case
 * @param wikipediaRepository   wikipedia repository
 * @param articleResponseMapper response mapper
 * @constructor produces instance of articles request use case
 * @author novikov
 *         Date: 14.09.2017
 */
@PerActivity
class RequestArticlesUseCase @Inject constructor(
        @OnlineDataSource private var wikipediaRepository: WikipediaRepository,
        private var articleResponseMapper: ArticleResponseMapper
) : AbstractUseCase<List<String>, RequestArticlesUseCase.RequestArticlesParams>() {

    override fun buildUseCaseObservable(params: RequestArticlesParams): Maybe<List<String>> {
        return wikipediaRepository.requestArticles(params.location).map(articleResponseMapper::transform)
    }

    /**
     * Request parameters
     * @param location wrapper around latitude and longitude
     * @constructor produces instance of request parameters
     */
    data class RequestArticlesParams(val location: WikiLocation)
}