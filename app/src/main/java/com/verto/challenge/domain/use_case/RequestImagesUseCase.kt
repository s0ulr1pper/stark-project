package com.verto.challenge.domain.use_case

import com.verto.challenge.injection.qualifier.OnlineDataSource
import com.verto.challenge.injection.scope.PerActivity
import io.reactivex.Maybe
import com.verto.challenge.domain.mapper.ImageResponseMapper
import com.verto.challenge.model.domain.LevenshteinEntity
import com.verto.challenge.network.WikipediaRepository
import javax.inject.Inject

/**
 * Request images use case
 * @param wikipediaRepository wikipedia repository
 * @param imageResponseMapper response mapper
 * @constructor produces instance of images request use case
 * @author novikov
 *         Date: 14.09.2017
 */
@PerActivity
class RequestImagesUseCase @Inject constructor(
        @OnlineDataSource private var wikipediaRepository: WikipediaRepository,
        private var imageResponseMapper: ImageResponseMapper
) : AbstractUseCase<List<LevenshteinEntity>, RequestImagesUseCase.RequestImagesParams>() {

    override fun buildUseCaseObservable(params: RequestImagesParams): Maybe<List<LevenshteinEntity>> {
        return wikipediaRepository.requestImages(params.imageIds).map(imageResponseMapper::transform)
    }

    /**
     * Request parameters
     * @param imageIds a list of image identifiers to request
     * @constructor produces instance of request parameters
     */
    data class RequestImagesParams(val imageIds: Iterable<String>)
}