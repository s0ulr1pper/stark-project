package com.verto.challenge.domain.mapper

import com.verto.challenge.model.domain.LevenshteinEntity
import com.verto.challenge.model.network.image.ImageResponse
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Image response mapper
 * @author novikov
 *         Date: 14.09.2017
 */
@Singleton
class ImageResponseMapper @Inject constructor() : DataMapper<ImageResponse, List<LevenshteinEntity>> {

    override fun transform(input: ImageResponse): List<LevenshteinEntity> {
        return input.query?.pages?.mapNotNull {
            it.value.images
        }?.flatMap {
            it
        }?.mapNotNull {
            it.title?.substringAfter("File:")?.substringBeforeLast(".")
        }.let { list ->
            val output = ArrayList<LevenshteinEntity>()
            list?.indices?.forEach { i ->
                (i + 1 until list.size).mapTo(output) {
                    LevenshteinEntity(list[i], list[it])
                }
            }
            output
        }
    }
}