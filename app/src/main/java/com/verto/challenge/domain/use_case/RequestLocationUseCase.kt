package com.verto.challenge.domain.use_case

import android.location.Criteria
import android.location.LocationManager
import com.verto.challenge.model.domain.WikiLocation
import io.reactivex.Maybe
import io.reactivex.rxkotlin.toMaybe
import javax.inject.Inject

/**
 * Request current location use case
 * @param locationManager location manager
 * @constructor produces instance of location request use case
 * @author novikov
 *         Date: 14.09.2017
 */
class RequestLocationUseCase @Inject constructor(
        private val locationManager: LocationManager
) : AbstractUseCase<WikiLocation, RequestLocationUseCase.RequestLocationParams>() {

    override fun buildUseCaseObservable(params: RequestLocationParams): Maybe<WikiLocation> {
        val criteria = Criteria().apply {
            accuracy = params.accuracy
            powerRequirement = params.powerRequirement
            isAltitudeRequired = params.isAltitudeRequired
            isBearingRequired = params.isBearingRequired
            isSpeedRequired = params.isSpeedRequired
            isCostAllowed = true
        }

        locationManager.getProviders(criteria, true)
                .mapNotNull {
                    locationManager.getLastKnownLocation(it)
                }
                .forEach {
                    return WikiLocation(it.latitude, it.longitude).toMaybe()
                }
        // i decided not to get string from resources here, because this is an exception
        // and with proper handling this would never be visible to user
        return Maybe.error<WikiLocation>(RuntimeException("Location is null"))
    }

    /**
     * Request parameters
     * @param accuracy accuracy
     * @param powerRequirement   power requirement
     * @param isAltitudeRequired whether require Z axis
     * @param isBearingRequired  whether require bearing info
     * @param isSpeedRequired    whether require bearing info
     * @constructor produces instance of request parameters
     */
    data class RequestLocationParams(
            val accuracy: Int = Criteria.ACCURACY_COARSE,
            val powerRequirement: Int = Criteria.NO_REQUIREMENT,
            val isAltitudeRequired: Boolean = false,
            val isBearingRequired: Boolean = false,
            val isSpeedRequired: Boolean = false)
}