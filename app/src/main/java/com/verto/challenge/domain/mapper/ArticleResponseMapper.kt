package com.verto.challenge.domain.mapper

import com.verto.challenge.model.network.article.ArticleResponse
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Article response mapper
 * @author novikov
 *         Date: 14.09.2017
 */
@Singleton
class ArticleResponseMapper @Inject constructor() : DataMapper<ArticleResponse, List<String>?> {

    override fun transform(input: ArticleResponse): List<String>? {
        return input.query?.articles?.mapNotNull {
            it.pageId
        }
    }
}