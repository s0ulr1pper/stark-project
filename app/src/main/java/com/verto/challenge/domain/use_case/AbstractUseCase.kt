package com.verto.challenge.domain.use_case

import io.reactivex.Maybe

/**
 * Abstract use case
 * @author novikov
 *         Date: 14.09.2017
 */
abstract class AbstractUseCase<T, in Params> {

    /**
     * Produce current use case observable
     * @param params use case parameters
     * @return current use case observable
     */
    abstract fun buildUseCaseObservable(params: Params): Maybe<T>

}