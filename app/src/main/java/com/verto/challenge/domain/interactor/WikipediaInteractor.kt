package com.verto.challenge.domain.interactor

import com.verto.challenge.injection.scope.PerActivity
import com.verto.challenge.domain.use_case.RequestArticlesUseCase
import com.verto.challenge.domain.use_case.RequestImagesUseCase
import com.verto.challenge.domain.use_case.RequestLocationUseCase
import com.verto.challenge.model.domain.LevenshteinEntity
import com.verto.challenge.model.domain.WikiLocation
import io.reactivex.Maybe
import io.reactivex.MaybeObserver
import io.reactivex.Scheduler
import java.util.concurrent.Executor
import javax.inject.Inject

/**
 * Wikipedia interactor
 * @param articlesUseCase request articles use case
 * @param imagesUseCase   request images use case
 * @param locationUseCase request current location
 * @constructor produces wikipedia interactor
 * @author novikov
 *         Date: 14.09.2017
 */
@PerActivity
class WikipediaInteractor @Inject constructor(
        private val articlesUseCase: RequestArticlesUseCase,
        private val imagesUseCase: RequestImagesUseCase,
        private val locationUseCase: RequestLocationUseCase,
        executor: Executor,
        scheduler: Scheduler
) : AbstractInteractor(executor, scheduler) {

    /**
     * Request nearest articles
     * @param observer observer
     * @param location current location
     * @return maybe observable with list of articles
     */
    fun requestArticles(observer: MaybeObserver<List<String>>, location: WikiLocation): Maybe<List<String>> {
        return execute(observer, articlesUseCase, RequestArticlesUseCase.RequestArticlesParams(location))
    }

    /**
     * Request nearest articles
     * @param observer observer
     * @param imageIds list of image identifiers
     * @return maybe observable with list of similarity entities
     */
    fun requestImages(observer: MaybeObserver<List<LevenshteinEntity>>, imageIds: Iterable<String>): Maybe<List<LevenshteinEntity>> {
        return execute(observer, imagesUseCase, RequestImagesUseCase.RequestImagesParams(imageIds))
    }

    /**
     * Request current location
     * @param observer observer
     * @return maybe observable with current location
     */
    fun requestLocation(observer: MaybeObserver<WikiLocation>): Maybe<WikiLocation> {
        return execute(observer, locationUseCase, RequestLocationUseCase.RequestLocationParams())
    }
}