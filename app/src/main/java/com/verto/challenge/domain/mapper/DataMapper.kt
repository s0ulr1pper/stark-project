package com.verto.challenge.domain.mapper

/**
 * Primitive interface for mapping data
 * @author novikov
 *         Date: 14.09.2017
 */
interface DataMapper<in IN, out OUT> {

    /**
     * Default mapping function
     * @param input in data
     * @return out data
     */
    fun transform(input: IN): OUT
}