package com.verto.challenge.network.transport

import io.reactivex.Maybe
import retrofit2.http.GET
import retrofit2.http.QueryMap
import com.verto.challenge.model.network.article.ArticleResponse
import com.verto.challenge.model.network.image.ImageResponse

/**
 * Retrofit API interface
 * @author novikov
 *         Date: 14.09.2017
 */
interface WikipediaService {

    /**
     * Request articles method
     * @param map query parameters
     * @return response observable
     */
    @GET("api.php")
    fun getArticles(@QueryMap map: Map<String, String>): Maybe<ArticleResponse>

    /**
     * Request images method
     * @param map query parameters
     * @return response observable
     */
    @GET("api.php")
    fun getImages(@QueryMap map: Map<String, String>): Maybe<ImageResponse>

}