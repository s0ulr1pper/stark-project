package com.verto.challenge.network.transport

import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Retrofit service generator
 * @author novikov
 *         14.09.2017
 */
object ServiceGenerator {

    /**
     * Get server url
     * @return url
     */
    private fun getUrl(): HttpUrl {
        return Retrofit.Builder()
                .baseUrl("https://en.wikipedia.org/w/")
                .build()
                .baseUrl()
    }

    /**
     * Produce a service
     * @param serviceClass service class
     * @return service
     */
    @Throws(RuntimeException::class)
    fun <S> createService(serviceClass: Class<S>): S {
        val builder = Retrofit.Builder().baseUrl(getUrl())
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        return builder.client(
                OkHttpClient.Builder().addInterceptor(HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BODY
                }).build()).build().create(serviceClass)
    }

}