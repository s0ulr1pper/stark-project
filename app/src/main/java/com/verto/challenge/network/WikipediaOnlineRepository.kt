package com.verto.challenge.network

import io.reactivex.Maybe
import com.verto.challenge.model.domain.WikiLocation
import com.verto.challenge.model.network.article.ArticleResponse
import com.verto.challenge.model.network.image.ImageResponse
import com.verto.challenge.network.transport.WikipediaService
import javax.inject.Inject

/**
 * Wikipedia online repository
 * @param wikipediaService wikipedia retrofit service
 * @constructor produces an instance of wikipedia online repository
 * @author novikov
 *         Date: 14.09.2017
 */
class WikipediaOnlineRepository @Inject constructor(
        private val wikipediaService: WikipediaService
) : WikipediaRepository {

    override fun requestArticles(location: WikiLocation): Maybe<ArticleResponse> {
        return wikipediaService.getArticles(
                mapOf(
                        "action" to "query",
                        "list" to "geosearch",
                        "gsradius" to "10000",
                        "gscoord" to "${location.latitude}|${location.longitude}",
                        "gslimit" to "50",
                        "format" to "json"
                ))
    }

    override fun requestImages(imageIds: Iterable<String>): Maybe<ImageResponse> {
        return wikipediaService.getImages(
                mapOf(
                        "action" to "query",
                        "prop" to "images",
                        "pageids" to imageIds.joinToString("|"),
                        "format" to "json"
                ))
    }
}