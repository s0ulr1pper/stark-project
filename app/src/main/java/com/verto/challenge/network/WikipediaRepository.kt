package com.verto.challenge.network

import com.verto.challenge.model.domain.WikiLocation
import com.verto.challenge.model.network.article.ArticleResponse
import com.verto.challenge.model.network.image.ImageResponse
import io.reactivex.Maybe

/**
 * Wikipedia repository protocol
 * @author novikov
 *         14.09.2017
 */
interface WikipediaRepository {

    /**
     * Request articles
     * @param location location
     * @return response observable
     */
    fun requestArticles(location: WikiLocation): Maybe<ArticleResponse>

    /**
     * Request images
     * @param imageIds a list of image identifiers
     * @return response observable
     */
    fun requestImages(imageIds: Iterable<String>): Maybe<ImageResponse>

}