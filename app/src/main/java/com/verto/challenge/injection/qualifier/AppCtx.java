package com.verto.challenge.injection.qualifier;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Qualifier;

/**
 * Application context qualifier
 * @author novikov
 *         Date: 14.09.2017
 */
@Qualifier
@Retention(RetentionPolicy.RUNTIME)
public @interface AppCtx {}
