package com.verto.challenge.injection;

import android.content.Context;
import android.location.LocationManager;

import java.util.concurrent.Executor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import com.verto.challenge.injection.qualifier.AppCtx;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import com.verto.challenge.MyApplication;
import com.verto.challenge.domain.JobExecutor;
import com.verto.challenge.network.transport.ServiceGenerator;
import com.verto.challenge.network.transport.WikipediaService;

/**
 * Application scope module
 * @author novikov
 *         Date: 14.09.2017
 */
@Module
public class ApplicationModule {

    /** Application instance */
    private final MyApplication application;

    /**
     * Constructor
     * @param application application instance
     */
    public ApplicationModule(MyApplication application) {
        this.application = application;
    }

    /**
     * Application context provider
     * @return application context
     */
    @Provides
    @Singleton
    @AppCtx
    public Context provideContext() {
        return application;
    }

    /**
     * Wikipedia service provider
     * @return wikipedia service
     */
    @Provides
    @Singleton
    public WikipediaService provideArticlesService() {
        return ServiceGenerator.INSTANCE.createService(WikipediaService.class);
    }

    /**
     * Location manager provider
     * @return location manager
     */
    @Provides
    @Singleton
    public LocationManager provideConnectivityManager() {
        return (LocationManager) application.getSystemService(Context.LOCATION_SERVICE);
    }

    /**
     * Executor provider
     * @param jobExecutor job executor
     * @return executor
     */
    @Provides
    @Singleton
    Executor provideThreadExecutor(JobExecutor jobExecutor) {
        return jobExecutor;
    }

    /**
     * Post execution thread provider
     * @return post execution thread
     */
    @Provides
    @Singleton
    Scheduler providePostExecutionThread() {
        return AndroidSchedulers.mainThread();
    }
}
