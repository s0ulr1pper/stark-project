package com.verto.challenge.injection.qualifier;

import javax.inject.Qualifier;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Qualifier for offline data source
 * @author novikov
 *         Date: 14.09.2017
 */
@Qualifier
@Retention(RetentionPolicy.RUNTIME)
public @interface OfflineDataSource {}
