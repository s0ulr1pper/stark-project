package com.verto.challenge.injection;

import dagger.Component;
import com.verto.challenge.injection.scope.PerActivity;
import com.verto.challenge.view.ArticlesActivity;

/**
 * Activity scope component
 * @author novikov
 *         Date: 14.09.2017
 */
@PerActivity
@Component(
        dependencies = ApplicationComponent.class,
        modules = ActivityModule.class
)
public interface ActivityComponent {

    /**
     * Inject into {@link ArticlesActivity}
     * @param activity instance of {@link ArticlesActivity}
     */
    void inject(ArticlesActivity activity);

}
