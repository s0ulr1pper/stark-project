package com.verto.challenge.injection.scope;

import javax.inject.Scope;

import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Activity scope
 * @author novikov
 *         Date: 14.09.2017
 */
@Scope
@Retention(RUNTIME)
public @interface PerActivity {}