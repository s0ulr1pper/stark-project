package com.verto.challenge.injection;

import android.content.Context;
import android.location.LocationManager;

import com.verto.challenge.domain.mapper.ArticleResponseMapper;
import com.verto.challenge.domain.mapper.ImageResponseMapper;
import com.verto.challenge.injection.qualifier.AppCtx;
import com.verto.challenge.network.transport.WikipediaService;

import java.util.concurrent.Executor;

import javax.inject.Singleton;

import dagger.Component;
import io.reactivex.Scheduler;

/**
 * Application scope component
 * @author novikov
 *         Date: 14.09.2017
 */
@Singleton
@Component(
        modules = ApplicationModule.class
)
public interface ApplicationComponent {

    /**
     * Getter for wikipedia service
     * @return wikipedia service
     */
    WikipediaService wikipediaService();

    /**
     * Getter for location manager
     * @return location manager
     */
    LocationManager locationManager();

    /**
     * Getter for application context
     * @return application context
     */
    @AppCtx
    Context appContext();

    /**
     * Getter for executor
     * @return executor
     */
    Executor executor();

    /**
     * Getter for scheduler
     * @return scheduler
     */
    Scheduler scheduler();

    /**
     * Getter for article response mapper
     * @return articles response mapper
     */
    ArticleResponseMapper articleResponseMapper();

    /**
     * Getter for image response mapper
     * @return image response mapper
     */
    ImageResponseMapper imageResponseMapper();

}
