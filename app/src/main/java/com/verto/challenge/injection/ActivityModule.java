package com.verto.challenge.injection;

import android.support.v7.app.AppCompatActivity;

import com.verto.challenge.injection.qualifier.OnlineDataSource;
import com.verto.challenge.injection.scope.PerActivity;
import com.verto.challenge.network.WikipediaOnlineRepository;
import com.verto.challenge.network.WikipediaRepository;
import com.verto.challenge.network.transport.WikipediaService;

import dagger.Module;
import dagger.Provides;

/**
 * Activity scope module
 * @author novikov
 *         Date: 14.09.2017
 */
@Module
public class ActivityModule {

    /** Attached activity to scope */
    private final AppCompatActivity attachedActivity;

    /**
     * Constructor
     * @param attachedActivity attached activity
     */
    public ActivityModule(AppCompatActivity attachedActivity) {
        this.attachedActivity = attachedActivity;
    }

    /**
     * Wikipedia repository provider
     * @param wikipediaService wikipedia retrofit service
     * @return Wikipedia repository
     */
    @Provides
    @PerActivity
    @OnlineDataSource
    public WikipediaRepository provideArticlesDataSource(WikipediaService wikipediaService) {
        return new WikipediaOnlineRepository(wikipediaService);
    }

    /**
     * Attached activity provider
     * @return attached activity
     */
    @Provides
    @PerActivity
    public AppCompatActivity provideAttachedActivity() {
        return attachedActivity;
    }

}
