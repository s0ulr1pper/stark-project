package com.verto.challenge.view

import android.Manifest
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.verto.challenge.MyApplication
import com.verto.challenge.R
import com.verto.challenge.databinding.ActivityArticlesBinding
import com.verto.challenge.injection.ActivityModule
import com.verto.challenge.injection.DaggerActivityComponent
import com.verto.challenge.presentation.ArticleViewModel
import com.verto.challenge.util.actionWithPermission
import com.verto.challenge.util.isPermissionGranted
import javax.inject.Inject

/**
 * Main, primary, single, sole, lonely activity for Verto :)
 * @author novikov
 *         14.09.2017
 */
class ArticlesActivity : AppCompatActivity(), ArticleView {

    /** Request code for location permission */
    private val REQUEST_LOCATION_PERMISSION_CODE = 1

    /** Binding */
    private lateinit var binding: ActivityArticlesBinding

    /** View model */
    @field:[Inject]
    lateinit var articleViewModel: ArticleViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val comp = (application as MyApplication).applicationComponent
        DaggerActivityComponent.builder().applicationComponent(comp).activityModule(ActivityModule(this)).build().inject(this)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_articles)
        binding.viewModel = articleViewModel
        articleViewModel.attachView(this)

        binding.swipeContainer.setOnRefreshListener {
            actionWithPermission(Manifest.permission.ACCESS_FINE_LOCATION, REQUEST_LOCATION_PERMISSION_CODE, articleViewModel::onLocationRequested)
        }
        actionWithPermission(Manifest.permission.ACCESS_FINE_LOCATION, REQUEST_LOCATION_PERMISSION_CODE, articleViewModel::onLocationRequested)
    }

    override fun onDestroy() {
        super.onDestroy()
        articleViewModel.destroy()
    }

    override fun onError(error: String?) {
        // base implementation
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show()
        onFinish()
    }

    override fun onSuccess() {
        // to be implemented
        onFinish()
    }

    override fun onFinish() {
        // to be implemented
        binding.swipeContainer.isRefreshing = false
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_LOCATION_PERMISSION_CODE -> {
                if (!isPermissionGranted(grantResults)) {
                    onError(getString(R.string.permission_location_error))
                } else {
                    articleViewModel::onLocationRequested
                }
            }
        }
    }
}
