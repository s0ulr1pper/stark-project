package com.verto.challenge.view

/**
 * View interface for any article view representation
 * @author novikov
 *         Date: 14.09.2017
 */
interface ArticleView {

    /**
     * Error handling
     * @param error error message
     */
    fun onError(error: String?)

    /** Success handling */
    fun onSuccess()

    /** In theory this should be invoke on both error or success handling */
    fun onFinish()
}